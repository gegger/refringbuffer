RefRingBuf
==========

(Circular) ring buffer implemented in C++ for use in embedded software (e.g. 
Arduino) with the possibility to cross-link the buffers. Many other 
implementations of circular buffers are lacking this feature, which allows
to cross-link buffers, e.g. one wants to store bursts of variable length and 
wants to keep track of their begin and their end in another buffer.

The memory required to hold the buffer are allocated by the constructor of the
class. Note that currently it is not possible to free or change the memory 
space afterwards, as the buffer is explicitly designed for embedded systems, 
where a deterministic runtime behaviour is important.

Errors are handled by boolean return values of the function calls instead of
exceptions, again driven by the focus on embedded environments.

By default the buffer works with unsigned 8 bit integers. On Arduino, this
correponds to uint8_t and to unsigned char otherwise. This behavious can
be changed by defining the REFRINGBUF_VALUE_T macro, albeit untested.

The buffer also uses 8 bit for indexing, which limits its length. In case
larger buffers are required, the data type for indexing can be changed with 
the REFRINGBUF_INDEX_T macro.

Apart from Arduino IDE, the library compiles also with Visual Studio (tested
with cmake, see test directory) and should also get along with gcc.

Example (basic usage):

	#include <RefRingBuf.h>

	value_t a, b, c = 10;

	RefRingBuf buf(c);

	for (a=0; a<c; a++) {
		buf.push(a, b);
		// value a was stored at position b
	}

	while (buf.pop(a, b)) {
		// value a taken from position b
	}




