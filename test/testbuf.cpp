#include "../RefRingBuf.h"
#define N 5

using namespace std;

int main() {
	RefRingBuf buf(N);

	unsigned char pos;
	unsigned char i;

	printf("capacity() -> %d\n", buf.capacity());

	for (i = 0; i < N; i++) {
		if (buf.push(i, pos)) {
			printf("push(%d) -> true (success)\n", i);
		}
		else {
			printf("push(%d) -> false (error)\n", i);
			return -1;
		}
	}

	// now the buffer is full

	if (buf.full()) {
		printf("full() -> true (as expected)\n");
	} else {
		printf("full() -> false (failed detecting full buffer)\n");
		return -1;
	}

	if (!buf.push(N, pos)) {
		printf("push() -> false (as expected)\n");
	} else {
		printf("push() on full buffer failed\n");
		return -1;
	}

	for (pos = 0; pos < N; pos++) {
		if (buf.at(pos, i)) {
			printf("at(%d) -> %d (success)\n", pos, i);
		}
		else {
			printf("at(%d) (error)\n", pos);
			return -1;
		}
	}

	if (!buf.at(N, pos)) {
		printf("at(%d) -> false (as expected, exceeds buffer capacity)\n", N);
	}
	else {
		printf("at(%d) (didnt fail as expected)\n", N);
		return -1;
	}

	for (i = 0; i < N; i++) {
		if (buf.pop(i, pos)) {
			printf("pop() -> %d (success)\n", i);
		} else {
			printf("pop() (error)\n");
			return -1;
		}
	}

	buf.printInternals();

	for (i = N; i < 2 * N; i++) {
		if (buf.push(i, pos)) {
			printf("push(%d) -> true (success)\n", i);
		}
		else {
			printf("push(%d) -> false (error)\n", i);
			return -1;
		}
	}

	for (pos = 0; pos < N; pos++) {
		if (buf.pop(i, pos)) {
			printf("pop() -> %d (success)\n", i);
		}
		else {
			printf("pop() (error)\n");
			return -1;
		}
	}

	buf.printInternals();

	return 0;
}