/*

The MIT License (MIT)

Copyright 2017 Georg Egger

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#ifndef REFRINGBUF_H
#define REFRINGBUF_H 0

#ifndef Arduino_h
	#include <iostream>	
	using namespace std;
	#define uint8_t unsigned char
#endif

#ifndef REFRINGBUF_INDEX_T
	typedef uint8_t index_t;	// default value
#else
	typedef REFRINGBUF_INDEX_T index_t;
#endif
#ifndef REFRINGBUF_VALUE_T
	typedef uint8_t value_t;	// default value
#else
	typedef REFRINGBUF_VALUE_T value_t;
#endif

class RefRingBuf {
public:

    RefRingBuf(const index_t capacity) {
    	buffer_ = (value_t*) malloc(sizeof(value_t)*capacity);
    	capacity_ = capacity;
        clear();
    }

    index_t size() const {
     	return size_;
     }

    bool empty() const { 
    	return size() == 0;
    }

    bool full() const { 
    	return size_ == capacity_;
    }

    index_t free() const {
    	return capacity_ - size_;
    }

    bool front(value_t &value) const {
    	if (empty()) return false;
    	value = buffer_[modulo_minus(firstEmpty_, size_)];
    	return true;
    }

    bool back(value_t &value) const {
    	if (empty()) return false;
    	value = buffer_[modulo_minus(firstEmpty_, 1)];
    	return true;
    }

    void clear() {
    	memset(buffer_, 0, sizeof(value_t)*capacity_); // optional
    	size_ = 0;
    }

    bool at(index_t index, value_t &value) const {
    	if ((index < 0) || (index >= size_)) {
    		return false;
    	} else {
			value = buffer_[modulo_plus(modulo_minus(firstEmpty_, size_), index)];
    		return true;
    	}
    }

    index_t capacity() const {
    	return capacity_;
    }

    bool push(value_t value, index_t &position) {
		if (full())
			return false;
		position = firstEmpty_;
		buffer_[position] = value;
		firstEmpty_ = modulo_plus(firstEmpty_, 1);
		size_++;
		return true;
    }

    bool pop(value_t &value, index_t &position) {
    	if (empty())
    		return false;
		position = modulo_minus(firstEmpty_, size_);

		value = buffer_[position];
		size_--;
		return true;
    }

    // (a-b) % capacity
    index_t modulo_minus(index_t a, index_t b) const {
    	if (a>=b) {
    		return (a-b) % capacity_;
    	} else {
    		if ((a==0) && (b==capacity_)) {
				return 0;
    		} else {
				return capacity_ - ((b-a) % capacity_);
    		}
    	}
    }

	// (a+b) % capacity
    index_t modulo_plus(index_t a, index_t b) const {
		return (a+b) % capacity_;
    }

    void printInternals() {
    	index_t i;
#ifdef Arduino_h    	
    	Serial.print(F("size="));
    	Serial.print(size_);
    	Serial.print(F(", capacity="));
    	Serial.print(capacity_);
    	Serial.print(F(", firstEmpty="));
    	Serial.print(firstEmpty_);
		Serial.print(F(", buffer=["));
    	for (i=0; i<capacity_; i++) {
    		Serial.print(buffer_[i]);
    		if (i+1<capacity_)
    			Serial.print(F(", "));
    	}
    	Serial.println(F("]"));
#else
		printf("size=%d, capacity=%d, firstEmpty=%d, buffer=[", size_, capacity_, firstEmpty_);
		for (i = 0; i<capacity_; i++) {
			printf("%d%s", buffer_[i], (i + 1<capacity_) ? ", " : "");
		}
		printf("]\n");
#endif // Arduino_h
    }

private:
    value_t* buffer_;
	index_t size_ = 0;
	index_t firstEmpty_ = 0;
	index_t capacity_ = 0;
};

#endif // REFRINGBUF_H